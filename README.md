**NAND Co.**

This game only has two levels as a proof of concept but more levels can be easily added.

Only 2 of the blocks are available to use: green(wire) and red (nand gate).

The objectives for these levels are to build complex components out of the more simple components available to the player. The player must use the components
to connect the green rectangles (inputs) to the blue rectangle (output)  on the edges of the board. The components placed by the player must be configured in 
such a way that the states of the inputs and outputs match that of the gate indicated in the window title when the inputs are turned on and off. 
The inputs can be turned on and off by clicking on them.

To check if you have completed the level press the zero key. The game will change to the next level if you succeed.