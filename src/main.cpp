
#define SDL_MAIN_HANDLED 1
#include <SDL2/SDL.h>

#include <iostream>
#include <vector>

#include "board.hpp"
#include "utility.hpp"
#include "tile.hpp"


// TODO:
// * More Gates
// * Puzzles
// * Levels
// * BUG: Clicking outside the grid causes a SEGFAULT

// TODO: REMOVE PLOX
Color tile_colours[Tile::LENGTH];


#define NA false

struct LevelStruct {
    std::string title;

    uint32_t width;
    uint32_t height;

    bool unlockedBricks[Tile::LENGTH];

    Board::IOState config[4];
    std::vector<WinCond> winConditions;
};

LevelStruct AndGate {
    "And", 5, 5, 
    {true, true, true, false, false, false, false, false}, 
    {Board::OUTPUT, Board::INPUT, Board::DISABLED, Board::INPUT}, 
    {{false, false, NA, false}, 
     {false,  true, NA, false},
     {false, false, NA,  true},
     { true,  true, NA,  true}}
};

// LevelStruct XOR {
//     "XOR", 8, 8,
//     {

//     }
// }


struct Inventory {
    private:
    public:
        bool unlocked[Tile::LENGTH];

    Inventory(){

    }
    void draw(SDL_Renderer* renderer) {
        int amount_drawn = 0;
        SDL_Rect inventory = {toolbox_pos_x, toolbox_pos_y, toolbox_width, toolbox_height};
        SetRendererColor(renderer, white);
        SDL_RenderDrawRect(renderer, &inventory);

        for  (int i=0; i<Tile::LENGTH; i++) {
            Tile t;
            t.type = static_cast<Tile::Type>(i);
            t.enabled = false;
            t.draw(renderer, toolbox_pos_x + 4, toolbox_pos_y + 4 + (i*toolbox_width), toolbox_width - 8, toolbox_width - 8);

            // SDL_Rect inv_slot = {toolbox_pos_x, toolbox_pos_y + i*toolbox_width, toolbox_width, toolbox_width};
            // SetRendererColor(renderer, tile_colours[i]);
            // SDL_RenderFillRect(renderer, &inv_slot);
        }


    }
    void click(int mouse_pos_x, int mouse_pos_y, Cursor& cursor) {
        Tile::Type type;
        int slot = (mouse_pos_y - toolbox_pos_y) / toolbox_width;
        if (unlocked[slot])
            cursor.cursor_type = (Tile::Type)slot;
    }

};


Board load_level(LevelStruct& level, Inventory& in) {
    Board b(level.width, level.height);

    printf("Created board\n");
    
    for (int i = 0; i < 4; i++)
        b.sides[i] = level.config[i];
    
    printf("Configured IO\n");

    b.win_conditions = level.winConditions;
    
    printf("Win conditions loaded\n");

    for (int i = 0; i < Tile::LENGTH; i++)
        in.unlocked[i] = level.unlockedBricks[i];

    printf("Unlocked bricks loaded");

    return b;
}



int main() {
    SDL_SetMainReady();

    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_Window* window = SDL_CreateWindow("Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, 0);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    bool running = true;

    SDL_Event e;
    uint64_t current = SDL_GetPerformanceCounter();
    uint64_t previous = 0;
    double delta_time = 0;

    Inventory inventory;
    Cursor cursor;

    Board board = load_level(AndGate, inventory);

	// Board board(5,5);
    // //                               N       E     S     W
    // board.win_conditions.push_back({false, false, 0, false});
    // board.win_conditions.push_back({false, true, 0, false});
    // board.win_conditions.push_back({false, false, 0, true});
    // board.win_conditions.push_back({true, true, 0, true});

    // board.pieces[2][1].type = Tile::Type::WIRE;
    // board.pieces[2][2].type = Tile::Type::NAND;
    // board.pieces[1][2].type = Tile::Type::WIRE;
    // 
    // 


    // board.sides[Board::NORTH] = Board::OUTPUT;
    // board.sides[Board::EAST] = Board::INPUT;
    // board.sides[Board::SOUTH] = Board::DISABLED;
    // board.sides[Board::WEST] = Board::INPUT;


    while (running) {
        previous = current;
        current = SDL_GetPerformanceCounter();

        delta_time = (double)((current-previous)/(double)SDL_GetPerformanceFrequency());
        while(SDL_PollEvent(&e)) {
            //handle event e here

            if (e.type == SDL_QUIT)  {
                running = false;
                break;
            }

            if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_0) {
                    fprintf(stderr, "Checking\n");
                    Board new_board(board);
                    
                    if(check_win(board)) {
                        fprintf(stderr, "Success\n");
                        exit(0);
                    }
                }
            }

            if (e.type == SDL_MOUSEBUTTONDOWN) {
                if (cursor.click_area(e) == Cursor::AreaType::INVENTORY){
                    inventory.click(e.motion.x, e.motion.y, cursor);
                }

                if (cursor.click_area(e) == Cursor::AreaType::BOARD){
                    board.click(e.motion.x, e.motion.y, cursor);
                }
            }
        }
        
        inventory.draw(renderer);

        board.update(delta_time);
		board.draw(renderer);
        
        SDL_RenderPresent(renderer);
        SetRendererColor(renderer, black);
        SDL_RenderClear(renderer);
    }



    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}