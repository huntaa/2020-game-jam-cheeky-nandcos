#pragma once

#include <SDL2/SDL.h>

struct Tile {
    enum Type {
        EMPTY,
        WIRE,
        NAND,
        NOT,
        AND,
        XOR,
        OR,
        HADDER,
        LENGTH
    } type;

    static constexpr bool emits[] {
        false,
        true,
        false,
        true,
        true,
        true,
        false
    };

    bool enabled;
    bool prop;

    void draw(SDL_Renderer* renderer, int x, int y, int cell_width, int cell_height);
};