
#include "utility.hpp"

void SetRendererColor(SDL_Renderer* r, Color c) {
    SDL_SetRenderDrawColor(r, (Uint8)c.r, (Uint8)c.g, (Uint8)c.b, (Uint8)c.a);
}

Cursor::AreaType Cursor::click_area(SDL_Event &e){
        if((e.motion.x > toolbox_pos_x) && (e.motion.x < toolbox_pos_x + toolbox_width)) {
            if((e.motion.y > toolbox_pos_y) && (e.motion.y < toolbox_pos_y + toolbox_height)) {
                return INVENTORY;
            }
        } else if (e.motion.x > board_pos_x && e.motion.x <= board_pos_x + board_width) {
            if (e.motion.y > board_pos_y && e.motion.y <= board_pos_y + board_height) {
                return BOARD;
            }
        } else {
            return NONE;
        }
    }

