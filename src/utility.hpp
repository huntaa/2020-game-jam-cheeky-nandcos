#pragma once 

#include <cinttypes>
#include <SDL2/SDL.h>
#include <vector>

#include "tile.hpp"


const int toolbox_pos_x = 10;
const int toolbox_pos_y = 10;
const int toolbox_height = 585;
const int num_slots = 15;
const int toolbox_width = toolbox_height / num_slots;

const int board_pos_x = 100;
const int board_pos_y = 10;
const int board_width = 500;
const int board_height = 500;


union Color {
    struct {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t a;
    };
    uint32_t rgba;
};

const Color white  = {255, 255, 255, 255};
const Color black  = {  0,   0,   0, 255};

const Color grey20 = { 51,  51,  51, 255};
const Color grey40 = {102, 102, 102, 255};
const Color grey60 = {153, 153, 153, 255};
const Color grey80 = {204, 204, 204, 255};
const Color grey90 = {230, 230, 230, 255};

const Color yellow = {255, 255,   0, 255};

const Color red    = {255, 0, 0, 255};
const Color green  = {0, 255, 0, 255};
const Color cyan   = {0, 255, 255, 255};
const Color blue   = {0, 0, 255, 255};
const Color light_blue   = {103, 103, 255, 255};

const Color orange = {255, 155, 0, 255};
const Color purple = {0, 180, 255, 255};


const Color wire_on  = {53, 255, 53, 255};
const Color wire_off = {  0, 100,   0, 255};


void SetRendererColor(SDL_Renderer* r, Color c);


class Cursor {

public:
    enum AreaType {
        NONE,
        BOARD,
        INVENTORY
    };
    Cursor(): cursor_type(Tile::Type::EMPTY){};

    AreaType click_area(SDL_Event &e);
    Tile::Type cursor_type;

};