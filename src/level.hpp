
#include <cinttypes>
#include <string>

#include "board.hpp"
#include "tile.hpp"

const uint64_t HEADER_MAGIC = 0xfe12cb76;

struct Level {
    struct Header {
        uint64_t magic;
        Board::IOState sides[4];
        uint8_t board_width;
        uint8_t board_height;
    };

    Board* load_from_file(std::string filename);
    void write_to_file(std::string filename, Board b);
}; 