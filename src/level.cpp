#include "level.hpp"
#include <vector>
#include <cstdio>


Board* Level::load_from_file(std::string filename) {
    // First load the file boi
    FILE* f = fopen(filename.c_str(), "rb");
    Header* h = new Header();

    fread((void*)h, sizeof(Header), 1, f);

    if (h->magic == HEADER_MAGIC) {
        Board b(h->board_width, h->board_height);

        for (int i = 0; i < 4; i++) {
            b.sides[i] = h->sides[i];
        }

        for (int i = 0; i < h->board_width; i++) {
            for (int j = 0; j < h->board_height; j++) {
                fread((void*)&b.pieces[i][j], sizeof(Tile), 1, f);
            }
        }

    } else {
        return nullptr;
    }
    
    delete h;
}

void Level::write_to_file(std::string filename, Board b) {
    // First load the file boi
    FILE* f = fopen(filename.c_str(), "wb");
    Header* h = new Header();

    h->board_height = b.height;
    h->board_width = b.width;
    h->magic = HEADER_MAGIC;
    
    for (int i = 0; i < 4; i++) {
        h->sides[i] = b.sides[i];
    }

    for (int i = 0; i < h->board_width; i++) {
        for (int j = 0; j < h->board_height; j++) {
            fwrite((void*)&b.pieces[i][j], sizeof(Tile), 1, f);
        }
    }

    delete h;
}