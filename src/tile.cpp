
#include "tile.hpp"
#include "utility.hpp"

void Tile::draw(SDL_Renderer* renderer, int x, int y, int cell_width, int cell_height) {
    SDL_Rect r = {x + 2, y + 2, cell_width - 4, cell_height - 4};
    SDL_Rect indicator = {x + 2, y + 2, cell_width - 4, 5};

    switch (type) {
    case EMPTY:
        SetRendererColor(renderer, black);
        SDL_RenderFillRect(renderer, &r);
        break;
            
    case WIRE:
        if (enabled) {
            SetRendererColor(renderer, wire_on);
        } else {
            SetRendererColor(renderer, wire_off);
        }
        SDL_RenderFillRect(renderer, &r);
        break;

    case NAND:
        SetRendererColor(renderer, red);
        SDL_RenderFillRect(renderer, &r);

        SetRendererColor(renderer, enabled ? wire_on : wire_off);
        SDL_RenderFillRect(renderer, &indicator);
        break;

    case AND:
        SetRendererColor(renderer, yellow);
        SDL_RenderFillRect(renderer, &r);

        SetRendererColor(renderer, enabled ? wire_on : wire_off);
        SDL_RenderFillRect(renderer, &indicator);
        break;    

    case NOT:
        SetRendererColor(renderer, blue);
        SDL_RenderFillRect(renderer, &r);

        SetRendererColor(renderer, enabled ? wire_on : wire_off);
        SDL_RenderFillRect(renderer, &indicator);
        break;

    case XOR:
        SetRendererColor(renderer, cyan);
        SDL_RenderFillRect(renderer, &r);

        SetRendererColor(renderer, enabled ? wire_on : wire_off);
        SDL_RenderFillRect(renderer, &indicator);
        break;

    case OR:
        SetRendererColor(renderer, orange);
        SDL_RenderFillRect(renderer, &r);

        SetRendererColor(renderer, enabled ? wire_on : wire_off);
        SDL_RenderFillRect(renderer, &indicator);

    case HADDER:
        SetRendererColor(renderer, purple);
        SDL_RenderFillRect(renderer, &r);

        
    }     
}