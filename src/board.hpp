#pragma once 

#include <SDL2/SDL.h>
#include <vector>
#include <iostream>

#include "utility.hpp"
#include "tile.hpp"


struct WinCond;

struct Board {

    enum IOState : uint8_t{
        DISABLED,
        INPUT,
        OUTPUT
    };

    enum Side {
        NORTH,
        EAST,
        SOUTH,
        WEST,
        N_SIDES
    };

    std::vector<WinCond> win_conditions;


    IOState sides[4];
    bool side_enabled[4];

	int width;
	int height;
	std::vector<std::vector<Tile>> pieces;

	Board(int w, int h);
    void update_wire(int i, int j);

    void step();

    void click(int x, int y, Cursor& cursor);

    void update(double deltaTime);
	void draw(SDL_Renderer* renderer);
};


struct WinCond{
    bool win_cond[Board::Side::N_SIDES];
};


bool check_win(Board board_copy);