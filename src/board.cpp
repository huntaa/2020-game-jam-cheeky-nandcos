
#include "board.hpp"	
    
Board::Board(int w, int h) {
    width = w;
    height = h;

    pieces.resize(width, std::vector<Tile>(height, Tile{Tile::Type::EMPTY, false}));
    for (int i = 0; i < 4; i++) {
        side_enabled[i] = false;
    }
}


void Board::update_wire(int i, int j) {

    if (pieces[i][j].type != Tile::WIRE) return;
    if (pieces[i][j].prop) return;

    pieces[i][j].enabled = true;
    pieces[i][j].prop = true;

    if(i != 0) update_wire(i-1, j);
    if(i != width - 1) update_wire(i+1, j);
    if(j != 0) update_wire(i, j-1);
    if (j!= height - 1) update_wire(i, j+1);
}


void Board::update(double deltaTime) {
    const float stepTime = .5;
    static float accum = 0.0;
    accum += deltaTime;

    if (accum >= stepTime) {
        accum = accum - stepTime;
        
       step();
    }
}


void Board::click(int x, int y, Cursor& cursor) {

    int cell_width = (board_width - 2) / width;
    int cell_height = (board_height - 2) / height;

    SDL_Point cur = {x, y};

    SDL_Rect north = {board_pos_x + board_width / 2 - (cell_width / 2) + 3, board_pos_y, cell_width - 4, 8};
    SDL_Rect south = {board_pos_x + board_width / 2 - (cell_width / 2) + 3, board_pos_y + board_height - 8, cell_width - 4, 8};
    SDL_Rect east = {board_pos_x, board_pos_y + board_height / 2 - (cell_height / 2) + 3, 8, cell_height - 4};
    SDL_Rect west = {board_pos_x + board_width - 8, board_pos_y + board_height / 2 - (cell_height / 2) + 3, 8, cell_height - 4};

    if (sides[NORTH] == INPUT && SDL_PointInRect(&cur, &north)) {
        side_enabled[NORTH] = !side_enabled[NORTH];
    } else if (sides[SOUTH] == INPUT && SDL_PointInRect(&cur, &south)) {
        side_enabled[SOUTH] = !side_enabled[SOUTH];
    } else if (sides[EAST] == INPUT && SDL_PointInRect(&cur, &east)) {
        side_enabled[EAST] = !side_enabled[EAST];
    } else if (sides[WEST] == INPUT && SDL_PointInRect(&cur, &west)) {
        side_enabled[WEST] = !side_enabled[WEST];
    } else {
        x -= board_pos_x;
        y -= board_pos_y;

        int x_tile = x / cell_width;
        int y_tile = y / cell_width;

        pieces[x_tile][y_tile].type = cursor.cursor_type;
        pieces[x_tile][y_tile].enabled = false;
    }
}


void Board::step() {
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            if (pieces[i][j].type == Tile::WIRE)
                pieces[i][j].prop = false;
        }
    }

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            Tile& t = pieces[i][j];
            Tile* l = i == 0 ? nullptr : &pieces[i-1][j];
            Tile* r = i == width - 1 ? nullptr : &pieces[i+1][j];
            Tile* u = j == 0 ? nullptr : &pieces[i][j-1];
            Tile* d = j == height - 1 ? nullptr : &pieces[i][j+1];

            switch(t.type) {
                case Tile::Type::NAND:
                    t.enabled = !(l && l->enabled && r && r->enabled);
                    if(t.enabled) update_wire(i, j-1);
                    break;

                case Tile::Type::AND:
                    t.enabled = l && l->enabled && r && r->enabled;
                    if(t.enabled) update_wire(i, j-1);
                    break;

                case Tile::Type::XOR:
                    t.enabled = ((l && l->enabled ) ^ (r && r->enabled));
                    if(t.enabled) update_wire(i, j-1);
                    break;

                case Tile::Type::NOT:
                    t.enabled = (!(d && d->enabled));
                    if(t.enabled) update_wire(i, j-1);
                    break;

                case Tile::Type::OR:
                    t.enabled = ((l && l->enabled) || (r && r->enabled));
                    if(t.enabled) update_wire(i, j-1);
                    break;

                case Tile::Type::HADDER:
                    //t.enabled = ((d ))
                    if((d && d->enabled) ^ (l && l->enabled)) {
                        update_wire(i, j-1);
                    } 
                    if ((d && d->enabled) && (l && l->enabled)) {
                        update_wire(i+1, j);
                    }
                    break;
            }
        }
    }

    if (sides[NORTH] == INPUT && side_enabled[NORTH]) {
        update_wire(width / 2, 0);
    }

    if (sides[SOUTH] == INPUT && side_enabled[SOUTH]) {
        update_wire(width / 2, height - 1);
    }

    if (sides[EAST] == INPUT && side_enabled[EAST]) {
        update_wire(0, height / 2);
    }

    if (sides[WEST] == INPUT && side_enabled[WEST]) {
        update_wire(width - 1, height / 2);
    }

    /*for (int i=0; i<4; i++) {
        if (sides[i] == OUTPUT && side_enabled[i]) {
            update_wire(0, 0);
        }
    }*/

    if (sides[NORTH] == OUTPUT) {
        side_enabled[NORTH] = pieces[width / 2][0].enabled;
    }

    if (sides[SOUTH] == OUTPUT) {
        side_enabled[SOUTH] = pieces[width / 2][height - 1].enabled;
    }

    if (sides[EAST] == OUTPUT) {
        side_enabled[EAST] = pieces[0][height / 2].enabled;
    }

    if (sides[WEST] == OUTPUT) {
        side_enabled[WEST] = pieces[width - 1][height / 2].enabled;
    }
    


    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            if (pieces[i][j].type == Tile::WIRE && pieces[i][j].prop == false) {
                pieces[i][j].enabled = false;
            }
                
        }
    }
}


void Board::draw(SDL_Renderer* renderer) {
    int cell_width = (board_width - 2) / width;
    int cell_height = (board_height - 2) / height;

    SDL_Rect background = {board_pos_x, board_pos_y, board_width, board_height};
    SetRendererColor(renderer, white);
    SDL_RenderFillRect(renderer, &background);

    SDL_Rect side_rects[] = {
        {board_pos_x + board_width / 2 - (cell_width / 2) + 3, board_pos_y, cell_width - 4, 8},
        {board_pos_x, board_pos_y + board_height / 2 - (cell_height / 2) + 3, 8, cell_height - 4},
        {board_pos_x + board_width / 2 - (cell_width / 2) + 3, board_pos_y + board_height - 8, cell_width - 4, 8},
        {board_pos_x + board_width - 8, board_pos_y + board_height / 2 - (cell_height / 2) + 3, 8, cell_height - 4}
    };

    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
            Tile& t = pieces[i][j];
            t.draw(renderer, board_pos_x + i*cell_width + 4, board_pos_y + j*cell_height + 4, cell_width, cell_height);
        }
    }

    for (int i = 0; i < 4; i++) {
        Side s = static_cast<Side>(i);

        if (sides[s] == INPUT) {
            SetRendererColor(renderer, side_enabled[s] ? wire_on : wire_off);
        } else {
            SetRendererColor(renderer, side_enabled[s] ? light_blue : blue);
        }

        if (sides[s] != DISABLED) {
            SDL_RenderFillRect(renderer, &side_rects[s]);
        }
    }
}

#define NUM_DIRECTIONS 4

bool satisfies(Board &board_copy, WinCond condition) {
    for(int j=0; j < NUM_DIRECTIONS; j++) {
        if(board_copy.sides[j] == Board::INPUT){
            board_copy.side_enabled[j] = condition.win_cond[j];
        }
    }
    for(int i=0; i < 30; i++) {
            board_copy.step();
    }
    for(int j=0; j < NUM_DIRECTIONS; j++) {
        for(int w=0; w<2; w++) {
            if(condition.win_cond[j] != board_copy.side_enabled[j]) {
                return false;
            }
            board_copy.step();
        } 
    }
    return true;
}

bool check_win(Board board_copy){
    for(int i=0; i<board_copy.win_conditions.size(); i++) {
        if(!satisfies(board_copy, board_copy.win_conditions[i])){
            return false;
        }
    }
    return true;
}